import React, { memo } from 'react';
import NextHead from 'next/head';


const Head = ({
    title,
    description,

}) => {
    return (
        <NextHead>
            <title>{title}</title>
            <meta
                property="og:description"
                content={description}
            />
        </NextHead>

    )
};

export default Head;