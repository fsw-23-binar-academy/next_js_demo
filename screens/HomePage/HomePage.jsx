import Button from 'react-bootstrap/Button';
import Table from 'react-bootstrap/Table';
import Page from '../../components/commons/page/Page';
import Link from 'next/link'
const HomePage = ({ posts }) => {
    
    return (
        <Page title={'blog of on your own'} description={'blog that you can write anything that you like'}>
            <h1>Home Page</h1>
            <Table striped bordered hover>
                <thead>
                    <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Body</th>
                    <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {posts?.map((post) => (
                        <tr key={post.id}>
                            <td>{post.id}</td>
                            <td> {post?.title} </td>
                            <td> {post?.body} </td>
                            <td>
                                <Button variant="secondary">
                                    <Link as={`/blog/${post.id}`} href="/blog/[slug]">
                                        
                                          <a style={{ color: 'white', textDecoration: 'none' }}>Detail</a>
                                    </Link>
                                </Button>
                            </td>
                        </tr>
                    ))}
                   
                </tbody>
            </Table>
        </Page>
    )
}

HomePage.getInitialProps = async (ctx, screen) => {
    const res = await fetch('https://jsonplaceholder.typicode.com/posts')
    const posts = await res.json();
    return {
        posts
    }
}


export default HomePage;