import Page from "../../components/commons/page/Page";

const BlogDetailPage = ({ post }) => {
    console.log('post', post);
    return (
        <Page title={post?.title} description={post?.body}>
            <div>
                <h1>BlogDetailPage</h1>
            </div>
        </Page>
    )
}



BlogDetailPage.getInitialProps = async ({ query }, screen) => {
    console.log('query', query);
    const res = await fetch('https://jsonplaceholder.typicode.com/posts/' + query?.slug)
    const post = await res.json();
    return {
        query,
        post
    }
}
export default BlogDetailPage;